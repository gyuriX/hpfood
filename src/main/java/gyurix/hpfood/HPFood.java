package gyurix.hpfood;

import gyurix.configfile.ConfigFile;
import gyurix.spigotlib.SU;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

import static org.bukkit.event.block.Action.RIGHT_CLICK_AIR;
import static org.bukkit.event.block.Action.RIGHT_CLICK_BLOCK;

public class HPFood extends JavaPlugin implements Listener {
    public static ConfigFile kf;

    @EventHandler
    public void onEat(PlayerInteractEvent e) {
        Action a = e.getAction();
        if (Config.disabledWorlds.contains(e.getPlayer().getWorld().getName()) || !(a == RIGHT_CLICK_BLOCK || a == RIGHT_CLICK_AIR) || e.getItem() == null)
            return;
        if (e.getClickedBlock() != null && e.getClickedBlock().getType() == Material.SOIL)
            return;
        Double val = Config.foods.get(e.getItem().getType().name());
        if (val == null)
            return;
        e.setCancelled(true);
        Player plr = e.getPlayer();
        if (plr.getHealth() == plr.getMaxHealth())
            return;
        ItemStack target = removeOne(e.getItem());
        try {
            if (e.getHand() == EquipmentSlot.OFF_HAND)
                plr.getInventory().setItemInOffHand(target);
            else
                plr.getInventory().setItemInMainHand(target);
        } catch (Throwable err) {
            plr.setItemInHand(target);
        }
        plr.setHealth(Math.min(plr.getMaxHealth(), Math.max(0, plr.getHealth() + val)));
    }

    @Override
    public void onEnable() {
        SU.saveResources(this, "config.yml");
        kf = new ConfigFile(new File(getDataFolder() + File.separator + "config.yml"));
        kf.data.deserialize(Config.class);
        SU.pm.registerEvents(this, this);
    }

    @EventHandler
    public void onFoodLose(FoodLevelChangeEvent e) {
        e.setFoodLevel(20);
    }

    private ItemStack removeOne(ItemStack is) {
        if (is.getAmount() == 1)
            return null;
        is.setAmount(is.getAmount() - 1);
        return is;
    }
}
